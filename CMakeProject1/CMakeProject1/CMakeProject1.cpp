﻿#include <iostream>
#include <iomanip>
#include "CMakeProject1.h"

using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    int n; // размер матрицы
    cout << "Введите размер матрицы не более 100" << endl;
    cin >> n;
    if (n > 100) {
        cout << "вы ввели число более 100";
        return 0;
    }
    int mas[100][100] = { 0 };
    for (int i = 0; i < n; i++) {
        cout << "введите значения элемнетов строки №: " << i + 1 << endl;
        for (int j = 0; j < n; j++) {
            cin >> mas[i][j];
        }
    }


    int min = func::searchMin(mas, n);



    if (func::isNumberMinMoreThanOne(mas, n, min)) {

        if (func::searchSimpleNumbers(mas, n) > 1) {
            // сортируем
            func::sortir(mas, n);
            // выводим
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    cout << setw(4) << mas[i][j];
                }
                cout << endl;
            }
        }
        else {
            cout << "количество чисел, модули которых простые меньше 2" << endl;
        }
    }
    else {
        cout << "количество минимальных элементов равно 1" << endl;
    }

    return 0;
}