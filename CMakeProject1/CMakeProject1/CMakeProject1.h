﻿#pragma once
namespace func {
	void sortir(int array[100][100], int n);
	int searchSimpleNumbers(int mas[100][100], int n);
	int searchMin(int mas[100][100], int n);
	bool isNumberMinMoreThanOne(int mas[100][100], int n, int min);
}
