#include "CMakeProject1.h"
#include <cmath>


    void func::sortir(int array[100][100], int n) {
        for (int i = 1; i < n; ++i) {
            for (int r = 0; r < n - i; r++) {
                // ������� ������������ �������� �����
                int multR = 1;
                int multRplus = 1;
                for (int a = 0; a < n; a++) {
                    multR *= array[r][a];
                }
                for (int a = 0; a < n; a++) {
                    multRplus *= array[r + 1][a];
                }

                if (multR < multRplus) //���������� ������������ �����
                {
                    for (int z = 0; z < n; z++) {
                        int temp = array[r][z];
                        array[r][z] = array[r + 1][z];
                        array[r + 1][z] = temp;
                    }
                }
            }
        }
    }

    int func::searchSimpleNumbers(int mas[100][100], int n) {
        int numberSimple = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int temp = abs(mas[i][j]);
                bool flag = true;
                for (int k = 2; k < temp; k++) {
                    if (temp % k == 0) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    numberSimple++;
                }

            }
            if (numberSimple > 1) {
                break;
            }
        }
        return numberSimple;
    }

    int func::searchMin(int mas[100][100], int n) {
        int min = mas[0][0];// ��������� �������� ������������ ��������
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (mas[i][j] < min) {
                    min = mas[i][j];//  ����� ����������� �������
                }
            }
        }
        return min;
    }

    bool func::isNumberMinMoreThanOne(int mas[100][100], int n, int min) {
        int numberMin = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (mas[i][j] == min) {
                    numberMin++;// ����� ���������� ����������� ���������
                }
            }
        }
        return (numberMin > 1);
    }
